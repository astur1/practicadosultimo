﻿
USE persona;

-- 1 indicar el numero de ciudades que hay en la tabla ciudades
  SELECT COUNT(*) FROM ciudad c;

-- 2 indicar el nombre de las ciudades que tengan una poblacion por encima de la población media
  SELECT AVG( c.población) FROM ciudad c;
  SELECT c.nombre, c.población FROM ciudad c WHERE c.población>(SELECT AVG( c.población) FROM ciudad c);  
   
-- 3 indicar el nombre de las ciudades que tengan una poblacion por debajo de la población media
  SELECT AVG( c.población) FROM ciudad c; 
  SELECT c.nombre ciudades FROM ciudad c WHERE c.población<(SELECT AVG( c.población) FROM ciudad c);

-- 4 indicar el nombre de las ciudades con la población máxima
  SELECT MAX( c.población) FROM ciudad c;
  SELECT c.nombre, c.población FROM ciudad c WHERE c.población=( SELECT MAX( c.población) FROM ciudad c); 
  
-- 5 indicar el nombre de las ciudades con la población mínima
  SELECT MIN( c.población) FROM ciudad c;
  SELECT c.nombre, c.población FROM ciudad c WHERE c.población=(SELECT MIN( c.población) FROM ciudad c); 
  
-- 6 indicar el número de ciudades que tengan una población por encima de la población media
  SELECT AVG( c.población) FROM ciudad c;       
 SELECT COUNT(*) ciudad, c.población FROM ciudad c GROUP BY c.población HAVING c.población>(SELECT AVG( c.población) FROM ciudad c);

-- 7 indicarme el número de personas que viven en cada ciudad
  SELECT COUNT(*), p.ciudad FROM persona p GROUP BY p.ciudad;
  
-- 8 utilizando la tabla trabaja indicarme cuantas personas trabajan en cada una de las compañías
  SELECT COUNT(*), t.compañia FROM trabaja t GROUP BY t.compañia;
  
-- 9 indicarme la compañía que más trabajadores tiene
  SELECT COUNT(*), t.compañia FROM trabaja t GROUP BY t.compañia;
  SELECT MAX( c1.compañia) FROM (  SELECT COUNT(*), t.compañia FROM trabaja t GROUP BY t.compañia) c1; 
  
-- 10 indicarme el salario medio de cada una de las compañías
  SELECT AVG( t.salario) salario_medio, t.compañia FROM trabaja t GROUP BY t.compañia;
  
-- 11 listarme el nombre de las personas y la población de la ciudad donde viven
  SELECT p.nombre, c.población FROM persona p JOIN ciudad c ON c.nombre= p.ciudad;

-- 12 listar el nombre de las personas, la calle donde viven y la población de la ciudad donde viven
   SELECT p.nombre, c.población, p.calle FROM persona p JOIN ciudad c ON c.nombre= p.ciudad;

-- 13 listar el nombre de las personas, la calle donde viven y la ciudad donde está la compañía para la que trabaja
  SELECT p.nombre, p.calle,  FROM trabaja t JOIN compañia c ON t.compañia= c.nombre; 

-- 15 listarme el nombre de la persona y el nombre de su supervisor
  SELECT p.nombre, s.supervisor FROM persona p JOIN supervisa s ON p.nombre= s.persona; 
  
-- 16 listarme el nombre de la persona, el nombre de su supervisor y las ciudades donde residen cada una de ellos
  SELECT p1.nombre,p1.ciudad,p.nombre, p.ciudad  FROM persona p JOIN supervisa s ON p.nombre= s.supervisor JOIN persona p1 ON p1.nombre= s.persona;
  
-- 17 indicarme el número de ciudades distintas que hay en la tabla compañía
  SELECT DISTINCT c.ciudad FROM compañia c; 
  
-- 18  indicarme el número de ciudades distintas que hay en la tabla personas
  SELECT DISTINCT p.ciudad FROM persona p;
  
-- 19 indicarme el nombre de las personas que trabajan para fagor
  SELECT t.persona FROM trabaja t WHERE t.compañia='FAGOR';
  
-- 20 indicarme el nombre de las personas que no trabajan para fagor
  SELECT t.persona FROM trabaja t WHERE t.compañia<>'FAGOR';
  
-- 21 indicarme el nombre de las personas que trabajan para indra
  SELECT t.persona FROM trabaja t WHERE t.compañia='INDRA';

-- 22 indicarme el nombre de las personas que trabajan para fagor o para indra
  SELECT t.persona FROM trabaja t WHERE t.compañia='FAGOR' OR t.compañia='INDRA'; 
  
-- 23 listar la población donde vive cada persona, sus salarios, su nombre y la compañía para la que trabaja.
--   Ordenar la salida por nombre de la persona y por salario de forma descendente.
  SELECT c.población, t.salario, p.nombre, t.compañia FROM ciudad c JOIN persona p ON c.nombre= p.ciudad 
  JOIN trabaja t ON t.persona= p.nombre ORDER BY p.nombre, t.salario DESC;                  
                 
      
